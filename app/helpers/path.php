<?php

function getModuleName(string $moduleName) {
   return app_path('modules'.DS().$moduleName.DS());
}

function loadConfigFile(string $fileName, string $moduleName) {
   return getModuleName($moduleName).'config'.DS().$fileName.'.php';
}

function loadRoute(string $fileName, string $moduleName) {
   return getModuleName($moduleName).'routes'.DS().$fileName.'.php';
}

function loadViews(string $moduleName) {
   return getModuleName($moduleName).'resources'.DS().'views';
}

function loadLang(string $moduleName) {
   return getModuleName($moduleName).'resources'.DS().'lang';
}

function loadMigrations(string $moduleName) {
   return getModuleName($moduleName).'database'.DS().'migrations';
}

function DS() {
   return DIRECTORY_SEPARATOR;
}